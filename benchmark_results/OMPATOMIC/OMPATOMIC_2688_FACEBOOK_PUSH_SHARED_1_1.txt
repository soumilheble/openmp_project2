
Usage: ./main -f <graph file> -r [root] -n [num threads] -p [0-push/1-pull]
	-f <graph file.txt>
	-h [help]
	-r [root/source]: BFS 
	-p [algorithm direction] [0-1]-push/pull
	-n [num threads] default:max number of threads the system has
 -----------------------------------------------------
| File Name                                           | 
 -----------------------------------------------------
| /tmp/rmat_benchmark/FACEBOOK                        | 
 -----------------------------------------------------
| Number of Threads                                   | 
 -----------------------------------------------------
| 1                                                   | 
 -----------------------------------------------------
 -----------------------------------------------------
| New graph calculating size                          | 
 -----------------------------------------------------
 -----------------------------------------------------
| New Graph Created                                   | 
 -----------------------------------------------------
| 0.017017                                            | 
 -----------------------------------------------------
 -----------------------------------------------------
| Populate Graph with edges                           | 
 -----------------------------------------------------
 -----------------------------------------------------
| Time load edges to graph (Seconds)                  | 
 -----------------------------------------------------
| 0.015813                                            | 
 -----------------------------------------------------
 -----------------------------------------------------
| Radix Sort Graph                                    | 
 -----------------------------------------------------
 -----------------------------------------------------
| Time Sorting (Seconds)                              | 
 -----------------------------------------------------
| 0.006521                                            | 
 -----------------------------------------------------
 -----------------------------------------------------
| Map vertices to Edges                               | 
 -----------------------------------------------------
 -----------------------------------------------------
| Time Mapping (Seconds)                              | 
 -----------------------------------------------------
| 0.000592                                            | 
 -----------------------------------------------------
 *****************************************************
 -----------------------------------------------------
| BFS Algorithm (PUSH/PULL)                           | 
 -----------------------------------------------------
| PUSH                                                | 
 -----------------------------------------------------
| ROOT/SOURCE                                         | 
 -----------------------------------------------------
| 2688                                                | 
 -----------------------------------------------------
 -----------------------------------------------------
| Starting Breadth First Search (PUSH)                | 
 -----------------------------------------------------
| 2688                                                | 
 -----------------------------------------------------
| Iteration       | Nodes           | Time (Seconds)  | 
 -----------------------------------------------------
| TD 0            | 1               | 0.000000        | 
| TD 1            | 9               | 0.000011        | 
| TD 2            | 76              | 0.000005        | 
| TD 3            | 118             | 0.000011        | 
| TD 4            | 123             | 0.000013        | 
| TD 5            | 118             | 0.000017        | 
| TD 6            | 23              | 0.000013        | 
| TD 7            | 7               | 0.000004        | 
| TD 8            | 9               | 0.000002        | 
| TD 9            | 1               | 0.000002        | 
| TD 10           | 0               | 0.000001        | 
 -----------------------------------------------------
| No OverHead     | 485             | 0.000079        | 
 -----------------------------------------------------
 -----------------------------------------------------
| total           | 485             | 0.000106        | 
 -----------------------------------------------------
 -----------------------------------------------------
| Time BFS (Seconds)                                  | 
 -----------------------------------------------------
| 0.000113                                            | 
 -----------------------------------------------------
 -----------------------------------------------------
| Free Graph (Seconds)                                | 
 -----------------------------------------------------
| 0.000071                                            | 
 -----------------------------------------------------
