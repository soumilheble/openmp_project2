
Usage: ./main -f <graph file> -r [root] -n [num threads] -p [0-push/1-pull]
	-f <graph file.txt>
	-h [help]
	-r [root/source]: BFS 
	-p [algorithm direction] [0-1]-push/pull
	-n [num threads] default:max number of threads the system has
 -----------------------------------------------------
| File Name                                           | 
 -----------------------------------------------------
| /tmp/rmat_benchmark/RMAT22                          | 
 -----------------------------------------------------
| Number of Threads                                   | 
 -----------------------------------------------------
| 32                                                  | 
 -----------------------------------------------------
 -----------------------------------------------------
| New graph calculating size                          | 
 -----------------------------------------------------
 -----------------------------------------------------
| New Graph Created                                   | 
 -----------------------------------------------------
| 15.494134                                           | 
 -----------------------------------------------------
 -----------------------------------------------------
| Populate Graph with edges                           | 
 -----------------------------------------------------
 -----------------------------------------------------
| Time load edges to graph (Seconds)                  | 
 -----------------------------------------------------
| 15.471413                                           | 
 -----------------------------------------------------
 -----------------------------------------------------
| Radix Sort Graph                                    | 
 -----------------------------------------------------
 -----------------------------------------------------
| Time Sorting (Seconds)                              | 
 -----------------------------------------------------
| 1.713227                                            | 
 -----------------------------------------------------
 -----------------------------------------------------
| Map vertices to Edges                               | 
 -----------------------------------------------------
 -----------------------------------------------------
| Time Mapping (Seconds)                              | 
 -----------------------------------------------------
| 0.490424                                            | 
 -----------------------------------------------------
 *****************************************************
 -----------------------------------------------------
| BFS Algorithm (PUSH/PULL)                           | 
 -----------------------------------------------------
| PUSH                                                | 
 -----------------------------------------------------
| ROOT/SOURCE                                         | 
 -----------------------------------------------------
| 3009230                                             | 
 -----------------------------------------------------
 -----------------------------------------------------
| Starting Breadth First Search (PUSH)                | 
 -----------------------------------------------------
| 3009230                                             | 
 -----------------------------------------------------
| Iteration       | Nodes           | Time (Seconds)  | 
 -----------------------------------------------------
| TD 0            | 1               | 0.000000        | 
| TD 1            | 18092           | 0.005332        | 
| TD 2            | 516807          | 0.080892        | 
| TD 3            | 400713          | 0.149102        | 
| TD 4            | 26908           | 0.029409        | 
| TD 5            | 1021            | 0.001558        | 
| TD 6            | 35              | 0.000167        | 
| TD 7            | 1               | 0.000132        | 
| TD 8            | 0               | 0.000132        | 
 -----------------------------------------------------
| No OverHead     | 963578          | 0.266724        | 
 -----------------------------------------------------
 -----------------------------------------------------
| total           | 963578          | 0.266781        | 
 -----------------------------------------------------
 -----------------------------------------------------
| Time BFS (Seconds)                                  | 
 -----------------------------------------------------
| 0.284323                                            | 
 -----------------------------------------------------
 -----------------------------------------------------
| Free Graph (Seconds)                                | 
 -----------------------------------------------------
| 0.004653                                            | 
 -----------------------------------------------------
