#!/bin/bash

#BFS Parallel Variants (Also Results Folder Names)
PARALLEL_TYPE=( SERIAL OMPATOMIC GCCATOMIC OMPCRITICAL OMPLOCK )

#Benchmark Result Directory
BENCHMARK_DIRECTORY=.

#Script Run Log
RUN_LOG=run_log.txt

#Iteration Indexes
ITERS=( 1 2 )

#Threads
THREAD_COUNT=( 1 2 4 8 16 32 )

#Benchmark File
BENCHMARK_NAME=( TEST FACEBOOK WIKI RMAT18 RMAT19 RMAT20 RMAT21 RMAT22 )

#Push/Pull
BFS_VARIANT=( PUSH PULL )

#Push Variants
PUSH_VARIANT=( SHARED LOCAL )

#Benchmark Result Output Report
BENCHMARK_RESULT_REPORT=benchmark_results.csv

#Remove The CSV File, Create The CSV File
rm $BENCHMARK_DIRECTORY"/"BENCHMARK_RESULT_REPORT
touch $BENCHMARK_DIRECTORY"/"BENCHMARK_RESULT_REPORT

#Remove The Run Log File, Create The Run Log File
rm $BENCHMARK_DIRECTORY"/"RUN_LOG
touch $BENCHMARK_DIRECTORY"/"RUN_LOG

#SED BFS Iterations Start End Line and Iteration Counter
SED_ITER_STL=( 66 66 66 66 66 66 66 66 )
SED_ITER_EDL=( 68 76 73 74 74 75 74 74 )
SED_ITER_LNT=0

#SED BFS Time Line, AWK Column
SED_BFS_TIME=( 78 86 83 84 84 85 84 84 )
AWK_BFS_COLUMN=2

#Error Storage Array
ERROR_RUN_ARRAY=0

#Run Index Counter
RUN_INDEX_COUNTER=0

#Temp
#sed -n '66,68p' SERIAL_4_TEST_PUSH_1_1.txt | awk '{print $2,$3,$4,$5}'

#Write Headers to File
echo "benchmark, bfs_variant, parallel_type, push_variant, thread_count, bfs_time, filename" >> $BENCHMARK_DIRECTORY"/"$BENCHMARK_RESULT_REPORT

#Strip and Save Comparison Files

for benchmark_name in ${BENCHMARK_NAME[*]}
do
    for bfs_variant in ${BFS_VARIANT[*]}
    do
        for parallel_type in ${PARALLEL_TYPE[*]}
        do
            if [ "$parallel_type" = "${PARALLEL_TYPE[0]}" ]
            then
                for iter_num in ${ITERS[*]}
                do
                        echo "sed -n '${SED_ITER_STL[$SED_ITER_LNT]},${SED_ITER_EDL[$SED_ITER_LNT]}p' ${BENCHMARK_DIRECTORY}/${parallel_type}/${parallel_type}_*_${benchmark_name}_${bfs_variant}_1_${iter_num}.txt"
                        
                        #sed -n "${SED_ITER_STL[$SED_ITER_LNT]},${SED_ITER_EDL[$SED_ITER_LNT]}p" ${BENCHMARK_DIRECTORY}"/"${parallel_type}"/"${parallel_type}"_*_"${benchmark_name}"_"${bfs_variant}"_1_"${iter_num}".txt"
                        
                        #echo "${BENCHMARK_DIRECTORY}/${parallel_type}/${parallel_type}_*_${benchmark_name}_${bfs_variant}_1_${iter_num}.txt"
                        
                        #echo "-------------------------------------------------"
                done
            else
                if [ "$bfs_variant" == "${BFS_VARIANT[0]}" ]
                then
                    for push_variant in ${PUSH_VARIANT[*]}
                    do
                        for thread_count in ${THREAD_COUNT[*]}
                        do
                            for iter_num in ${ITERS[*]}
                            do
                                #echo "${BENCHMARK_DIRECTORY}/${parallel_type}/${parallel_type}_*_${benchmark_name}_${bfs_variant}_${push_variant}_${thread_count}_${iter_num}.txt"
                                #echo "-------------------------------------------------"
                                echo "A-------------------------------------------------"
                            done
                        done
                    done
                else
                    for thread_count in ${THREAD_COUNT[*]}
                    do
                        for iter_num in ${ITERS[*]}
                        do
                            #echo "${BENCHMARK_DIRECTORY}/${parallel_type}/${parallel_type}_*_${benchmark_name}_${bfs_variant}_${thread_count}_${iter_num}.txt"
                            #echo "-------------------------------------------------"
                            #echo "-------------------------------------------------"
                            echo "B-------------------------------------------------"
                        done
                    done
                fi    
            fi
        done
    done
    echo "-------------------------------------------------"
    echo "-------------------------------------------------"
    echo "-------------------------------------------------"
    echo "-------------------------------------------------"
    SED_ITER_LNT=$(( SED_ITER_LNT + 1 ))
done
