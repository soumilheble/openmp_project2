
Usage: ./main -f <graph file> -r [root] -n [num threads] -p [0-push/1-pull]
	-f <graph file.txt>
	-h [help]
	-r [root/source]: BFS 
	-p [algorithm direction] [0-1]-push/pull
	-n [num threads] default:max number of threads the system has
 -----------------------------------------------------
| File Name                                           | 
 -----------------------------------------------------
| /tmp/rmat_benchmark/WIKI                            | 
 -----------------------------------------------------
| Number of Threads                                   | 
 -----------------------------------------------------
| 32                                                  | 
 -----------------------------------------------------
 -----------------------------------------------------
| New graph calculating size                          | 
 -----------------------------------------------------
 -----------------------------------------------------
| New Graph Created                                   | 
 -----------------------------------------------------
| 0.019465                                            | 
 -----------------------------------------------------
 -----------------------------------------------------
| Populate Graph with edges                           | 
 -----------------------------------------------------
 -----------------------------------------------------
| Time load edges to graph (Seconds)                  | 
 -----------------------------------------------------
| 0.017984                                            | 
 -----------------------------------------------------
 -----------------------------------------------------
| Radix Sort Graph                                    | 
 -----------------------------------------------------
 -----------------------------------------------------
| Time Sorting (Seconds)                              | 
 -----------------------------------------------------
| 0.005803                                            | 
 -----------------------------------------------------
 -----------------------------------------------------
| Map vertices to Edges                               | 
 -----------------------------------------------------
 -----------------------------------------------------
| Time Mapping (Seconds)                              | 
 -----------------------------------------------------
| 0.000759                                            | 
 -----------------------------------------------------
 *****************************************************
 -----------------------------------------------------
| BFS Algorithm (PUSH/PULL)                           | 
 -----------------------------------------------------
| PUSH                                                | 
 -----------------------------------------------------
| ROOT/SOURCE                                         | 
 -----------------------------------------------------
| 1938                                                | 
 -----------------------------------------------------
 -----------------------------------------------------
| Starting Breadth First Search (PUSH)                | 
 -----------------------------------------------------
| 1938                                                | 
 -----------------------------------------------------
| Iteration       | Nodes           | Time (Seconds)  | 
 -----------------------------------------------------
| TD 0            | 1               | 0.000000        | 
| TD 1            | 1               | 0.000085        | 
| TD 2            | 12              | 0.000128        | 
| TD 3            | 370             | 0.000072        | 
| TD 4            | 1634            | 0.000350        | 
| TD 5            | 293             | 0.000328        | 
| TD 6            | 6               | 0.000083        | 
| TD 7            | 0               | 0.000074        | 
 -----------------------------------------------------
| No OverHead     | 2317            | 0.001120        | 
 -----------------------------------------------------
 -----------------------------------------------------
| total           | 2317            | 0.001148        | 
 -----------------------------------------------------
 -----------------------------------------------------
| Time BFS (Seconds)                                  | 
 -----------------------------------------------------
| 0.001231                                            | 
 -----------------------------------------------------
 -----------------------------------------------------
| Free Graph (Seconds)                                | 
 -----------------------------------------------------
| 0.000141                                            | 
 -----------------------------------------------------
