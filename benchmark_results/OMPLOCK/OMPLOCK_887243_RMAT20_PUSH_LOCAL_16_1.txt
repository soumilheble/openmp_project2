
Usage: ./main -f <graph file> -r [root] -n [num threads] -p [0-push/1-pull]
	-f <graph file.txt>
	-h [help]
	-r [root/source]: BFS 
	-p [algorithm direction] [0-1]-push/pull
	-n [num threads] default:max number of threads the system has
 -----------------------------------------------------
| File Name                                           | 
 -----------------------------------------------------
| /tmp/rmat_benchmark/RMAT20                          | 
 -----------------------------------------------------
| Number of Threads                                   | 
 -----------------------------------------------------
| 16                                                  | 
 -----------------------------------------------------
 -----------------------------------------------------
| New graph calculating size                          | 
 -----------------------------------------------------
 -----------------------------------------------------
| New Graph Created                                   | 
 -----------------------------------------------------
| 3.383816                                            | 
 -----------------------------------------------------
 -----------------------------------------------------
| Populate Graph with edges                           | 
 -----------------------------------------------------
 -----------------------------------------------------
| Time load edges to graph (Seconds)                  | 
 -----------------------------------------------------
| 3.363674                                            | 
 -----------------------------------------------------
 -----------------------------------------------------
| Radix Sort Graph                                    | 
 -----------------------------------------------------
 -----------------------------------------------------
| Time Sorting (Seconds)                              | 
 -----------------------------------------------------
| 0.414523                                            | 
 -----------------------------------------------------
 -----------------------------------------------------
| Map vertices to Edges                               | 
 -----------------------------------------------------
 -----------------------------------------------------
| Time Mapping (Seconds)                              | 
 -----------------------------------------------------
| 0.115558                                            | 
 -----------------------------------------------------
 *****************************************************
 -----------------------------------------------------
| BFS Algorithm (PUSH/PULL)                           | 
 -----------------------------------------------------
| PUSH                                                | 
 -----------------------------------------------------
| ROOT/SOURCE                                         | 
 -----------------------------------------------------
| 887243                                              | 
 -----------------------------------------------------
 -----------------------------------------------------
| Starting Breadth First Search (PUSH)                | 
 -----------------------------------------------------
| 887243                                              | 
 -----------------------------------------------------
| Iteration       | Nodes           | Time (Seconds)  | 
 -----------------------------------------------------
| TD 0            | 1               | 0.000000        | 
| TD 1            | 1               | 0.000132        | 
| TD 2            | 92              | 0.000114        | 
| TD 3            | 14752           | 0.003823        | 
| TD 4            | 169250          | 0.047472        | 
| TD 5            | 76158           | 0.035340        | 
| TD 6            | 4781            | 0.004330        | 
| TD 7            | 183             | 0.000268        | 
| TD 8            | 5               | 0.000041        | 
| TD 9            | 0               | 0.000039        | 
 -----------------------------------------------------
| No OverHead     | 265223          | 0.091559        | 
 -----------------------------------------------------
 -----------------------------------------------------
| total           | 265223          | 0.091605        | 
 -----------------------------------------------------
 -----------------------------------------------------
| Time BFS (Seconds)                                  | 
 -----------------------------------------------------
| 0.093923                                            | 
 -----------------------------------------------------
 -----------------------------------------------------
| Free Graph (Seconds)                                | 
 -----------------------------------------------------
| 0.001335                                            | 
 -----------------------------------------------------
