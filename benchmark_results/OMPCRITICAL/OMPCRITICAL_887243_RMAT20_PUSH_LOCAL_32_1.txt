
Usage: ./main -f <graph file> -r [root] -n [num threads] -p [0-push/1-pull]
	-f <graph file.txt>
	-h [help]
	-r [root/source]: BFS 
	-p [algorithm direction] [0-1]-push/pull
	-n [num threads] default:max number of threads the system has
 -----------------------------------------------------
| File Name                                           | 
 -----------------------------------------------------
| /tmp/rmat_benchmark/RMAT20                          | 
 -----------------------------------------------------
| Number of Threads                                   | 
 -----------------------------------------------------
| 32                                                  | 
 -----------------------------------------------------
 -----------------------------------------------------
| New graph calculating size                          | 
 -----------------------------------------------------
 -----------------------------------------------------
| New Graph Created                                   | 
 -----------------------------------------------------
| 3.378936                                            | 
 -----------------------------------------------------
 -----------------------------------------------------
| Populate Graph with edges                           | 
 -----------------------------------------------------
 -----------------------------------------------------
| Time load edges to graph (Seconds)                  | 
 -----------------------------------------------------
| 3.370150                                            | 
 -----------------------------------------------------
 -----------------------------------------------------
| Radix Sort Graph                                    | 
 -----------------------------------------------------
 -----------------------------------------------------
| Time Sorting (Seconds)                              | 
 -----------------------------------------------------
| 0.396116                                            | 
 -----------------------------------------------------
 -----------------------------------------------------
| Map vertices to Edges                               | 
 -----------------------------------------------------
 -----------------------------------------------------
| Time Mapping (Seconds)                              | 
 -----------------------------------------------------
| 0.113261                                            | 
 -----------------------------------------------------
 *****************************************************
 -----------------------------------------------------
| BFS Algorithm (PUSH/PULL)                           | 
 -----------------------------------------------------
| PUSH                                                | 
 -----------------------------------------------------
| ROOT/SOURCE                                         | 
 -----------------------------------------------------
| 887243                                              | 
 -----------------------------------------------------
 -----------------------------------------------------
| Starting Breadth First Search (PUSH)                | 
 -----------------------------------------------------
| 887243                                              | 
 -----------------------------------------------------
| Iteration       | Nodes           | Time (Seconds)  | 
 -----------------------------------------------------
| TD 0            | 1               | 0.000001        | 
| TD 1            | 1               | 0.000118        | 
| TD 2            | 92              | 0.000088        | 
| TD 3            | 14752           | 0.003211        | 
| TD 4            | 169250          | 0.040683        | 
| TD 5            | 76158           | 0.026040        | 
| TD 6            | 4781            | 0.003953        | 
| TD 7            | 183             | 0.000260        | 
| TD 8            | 5               | 0.000075        | 
| TD 9            | 0               | 0.000068        | 
 -----------------------------------------------------
| No OverHead     | 265223          | 0.074497        | 
 -----------------------------------------------------
 -----------------------------------------------------
| total           | 265223          | 0.074540        | 
 -----------------------------------------------------
 -----------------------------------------------------
| Time BFS (Seconds)                                  | 
 -----------------------------------------------------
| 0.079000                                            | 
 -----------------------------------------------------
 -----------------------------------------------------
| Free Graph (Seconds)                                | 
 -----------------------------------------------------
| 0.001363                                            | 
 -----------------------------------------------------
