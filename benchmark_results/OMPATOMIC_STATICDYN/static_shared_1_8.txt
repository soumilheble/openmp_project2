linking main <- main.o edgelist.o sort.o vertex.o timer.o bfs.o graph.o
./bin/main -f /tmp/rmat_benchmark/RMAT22 -n 8 -p 0 -r 3009230 -h

Usage: ./main -f <graph file> -r [root] -n [num threads] -p [0-push/1-pull]
	-f <graph file.txt>
	-h [help]
	-r [root/source]: BFS 
	-p [algorithm direction] [0-1]-push/pull
	-n [num threads] default:max number of threads the system has
 -----------------------------------------------------
| File Name                                           | 
 -----------------------------------------------------
| /tmp/rmat_benchmark/RMAT22                          | 
 -----------------------------------------------------
| Number of Threads                                   | 
 -----------------------------------------------------
| 8                                                   | 
 -----------------------------------------------------
 -----------------------------------------------------
| New graph calculating size                          | 
 -----------------------------------------------------
 -----------------------------------------------------
| New Graph Created                                   | 
 -----------------------------------------------------
| 15.056447                                           | 
 -----------------------------------------------------
 -----------------------------------------------------
| Populate Graph with edges                           | 
 -----------------------------------------------------
 -----------------------------------------------------
| Time load edges to graph (Seconds)                  | 
 -----------------------------------------------------
| 15.072450                                           | 
 -----------------------------------------------------
 -----------------------------------------------------
| Radix Sort Graph                                    | 
 -----------------------------------------------------
 -----------------------------------------------------
| Time Sorting (Seconds)                              | 
 -----------------------------------------------------
| 1.771222                                            | 
 -----------------------------------------------------
 -----------------------------------------------------
| Map vertices to Edges                               | 
 -----------------------------------------------------
 -----------------------------------------------------
| Time Mapping (Seconds)                              | 
 -----------------------------------------------------
| 0.475945                                            | 
 -----------------------------------------------------
 *****************************************************
 -----------------------------------------------------
| BFS Algorithm (PUSH/PULL)                           | 
 -----------------------------------------------------
| PUSH                                                | 
 -----------------------------------------------------
| ROOT/SOURCE                                         | 
 -----------------------------------------------------
| 3009230                                             | 
 -----------------------------------------------------
 -----------------------------------------------------
| Starting Breadth First Search (PUSH)                | 
 -----------------------------------------------------
| 3009230                                             | 
 -----------------------------------------------------
| Iteration       | Nodes           | Time (Seconds)  | 
 -----------------------------------------------------
| TD 0            | 1               | 0.000000        | 
| TD 1            | 18092           | 0.004316        | 
| TD 2            | 516807          | 0.112771        | 
| TD 3            | 400713          | 0.248846        | 
| TD 4            | 26908           | 0.024917        | 
| TD 5            | 1021            | 0.004289        | 
| TD 6            | 35              | 0.002896        | 
| TD 7            | 1               | 0.002904        | 
| TD 8            | 0               | 0.002904        | 
 -----------------------------------------------------
| No OverHead     | 963578          | 0.403843        | 
 -----------------------------------------------------
 -----------------------------------------------------
| total           | 963578          | 0.403890        | 
 -----------------------------------------------------
 -----------------------------------------------------
| Time BFS (Seconds)                                  | 
 -----------------------------------------------------
| 0.408685                                            | 
 -----------------------------------------------------
 -----------------------------------------------------
| Free Graph (Seconds)                                | 
 -----------------------------------------------------
| 0.004222                                            | 
 -----------------------------------------------------
