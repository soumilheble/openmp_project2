linking main <- main.o edgelist.o sort.o vertex.o timer.o bfs.o graph.o
./bin/main -f /tmp/rmat_benchmark/RMAT22 -n 2 -p 0 -r 3009230 -h

Usage: ./main -f <graph file> -r [root] -n [num threads] -p [0-push/1-pull]
	-f <graph file.txt>
	-h [help]
	-r [root/source]: BFS 
	-p [algorithm direction] [0-1]-push/pull
	-n [num threads] default:max number of threads the system has
 -----------------------------------------------------
| File Name                                           | 
 -----------------------------------------------------
| /tmp/rmat_benchmark/RMAT22                          | 
 -----------------------------------------------------
| Number of Threads                                   | 
 -----------------------------------------------------
| 2                                                   | 
 -----------------------------------------------------
 -----------------------------------------------------
| New graph calculating size                          | 
 -----------------------------------------------------
 -----------------------------------------------------
| New Graph Created                                   | 
 -----------------------------------------------------
| 15.336781                                           | 
 -----------------------------------------------------
 -----------------------------------------------------
| Populate Graph with edges                           | 
 -----------------------------------------------------
 -----------------------------------------------------
| Time load edges to graph (Seconds)                  | 
 -----------------------------------------------------
| 15.039350                                           | 
 -----------------------------------------------------
 -----------------------------------------------------
| Radix Sort Graph                                    | 
 -----------------------------------------------------
 -----------------------------------------------------
| Time Sorting (Seconds)                              | 
 -----------------------------------------------------
| 2.820680                                            | 
 -----------------------------------------------------
 -----------------------------------------------------
| Map vertices to Edges                               | 
 -----------------------------------------------------
 -----------------------------------------------------
| Time Mapping (Seconds)                              | 
 -----------------------------------------------------
| 0.478480                                            | 
 -----------------------------------------------------
 *****************************************************
 -----------------------------------------------------
| BFS Algorithm (PUSH/PULL)                           | 
 -----------------------------------------------------
| PUSH                                                | 
 -----------------------------------------------------
| ROOT/SOURCE                                         | 
 -----------------------------------------------------
| 3009230                                             | 
 -----------------------------------------------------
 -----------------------------------------------------
| Starting Breadth First Search (PUSH)                | 
 -----------------------------------------------------
| 3009230                                             | 
 -----------------------------------------------------
| Iteration       | Nodes           | Time (Seconds)  | 
 -----------------------------------------------------
| TD 0            | 1               | 0.000000        | 
| TD 1            | 18092           | 0.004248        | 
| TD 2            | 516807          | 0.132515        | 
| TD 3            | 400713          | 0.294759        | 
| TD 4            | 26908           | 0.059762        | 
| TD 5            | 1021            | 0.003128        | 
| TD 6            | 35              | 0.000119        | 
| TD 7            | 1               | 0.000006        | 
| TD 8            | 0               | 0.000002        | 
 -----------------------------------------------------
| No OverHead     | 963578          | 0.494539        | 
 -----------------------------------------------------
 -----------------------------------------------------
| total           | 963578          | 0.494581        | 
 -----------------------------------------------------
 -----------------------------------------------------
| Time BFS (Seconds)                                  | 
 -----------------------------------------------------
| 0.496311                                            | 
 -----------------------------------------------------
 -----------------------------------------------------
| Free Graph (Seconds)                                | 
 -----------------------------------------------------
| 0.004081                                            | 
 -----------------------------------------------------
