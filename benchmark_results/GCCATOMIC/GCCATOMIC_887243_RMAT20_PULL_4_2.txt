
Usage: ./main -f <graph file> -r [root] -n [num threads] -p [0-push/1-pull]
	-f <graph file.txt>
	-h [help]
	-r [root/source]: BFS 
	-p [algorithm direction] [0-1]-push/pull
	-n [num threads] default:max number of threads the system has
 -----------------------------------------------------
| File Name                                           | 
 -----------------------------------------------------
| /tmp/rmat_benchmark/RMAT20                          | 
 -----------------------------------------------------
| Number of Threads                                   | 
 -----------------------------------------------------
| 4                                                   | 
 -----------------------------------------------------
 -----------------------------------------------------
| New graph calculating size                          | 
 -----------------------------------------------------
 -----------------------------------------------------
| New Graph Created                                   | 
 -----------------------------------------------------
| 3.604232                                            | 
 -----------------------------------------------------
 -----------------------------------------------------
| Populate Graph with edges                           | 
 -----------------------------------------------------
 -----------------------------------------------------
| Time load edges to graph (Seconds)                  | 
 -----------------------------------------------------
| 3.639837                                            | 
 -----------------------------------------------------
 -----------------------------------------------------
| Radix Sort Graph                                    | 
 -----------------------------------------------------
 -----------------------------------------------------
| Time Sorting (Seconds)                              | 
 -----------------------------------------------------
| 0.498618                                            | 
 -----------------------------------------------------
 -----------------------------------------------------
| Map vertices to Edges                               | 
 -----------------------------------------------------
 -----------------------------------------------------
| Time Mapping (Seconds)                              | 
 -----------------------------------------------------
| 0.122611                                            | 
 -----------------------------------------------------
 *****************************************************
 -----------------------------------------------------
| BFS Algorithm (PUSH/PULL)                           | 
 -----------------------------------------------------
| PULL                                                | 
 -----------------------------------------------------
| ROOT/SOURCE                                         | 
 -----------------------------------------------------
| 887243                                              | 
 -----------------------------------------------------
 -----------------------------------------------------
| Starting Breadth First Search (PULL)                | 
 -----------------------------------------------------
| 887243                                              | 
 -----------------------------------------------------
| Iteration       | Nodes           | Time (Seconds)  | 
 -----------------------------------------------------
| BU 0            | 1               | 0.000005        | 
| BU 1            | 1               | 0.024029        | 
| BU 2            | 92              | 0.024105        | 
| BU 3            | 14752           | 0.015943        | 
| BU 4            | 169250          | 0.007152        | 
| BU 5            | 76158           | 0.004068        | 
| BU 6            | 4781            | 0.002463        | 
| BU 7            | 183             | 0.002342        | 
| BU 8            | 5               | 0.002269        | 
| BU 9            | 0               | 0.002305        | 
 -----------------------------------------------------
| No OverHead     | 265223          | 0.084681        | 
 -----------------------------------------------------
 -----------------------------------------------------
| total           | 265223          | 0.084752        | 
 -----------------------------------------------------
 -----------------------------------------------------
| Time BFS (Seconds)                                  | 
 -----------------------------------------------------
| 0.085082                                            | 
 -----------------------------------------------------
 -----------------------------------------------------
| Free Graph (Seconds)                                | 
 -----------------------------------------------------
| 0.001640                                            | 
 -----------------------------------------------------
