#!/bin/bash

#Serial BFS Test

PARALLEL_ALGO=SERIAL
DATA_SET_DIR="/tmp/rmat_benchmark"
RESULTS_DIR=$DATA_SET_DIR"/benchmark_results/"$PARALLEL_ALGO
DATA_SETS=( TEST FACEBOOK WIKI RMAT18 RMAT19 RMAT20 RMAT21 RMAT22 )
SOURCE_NODES=( 4 2688 1938 64286 7356 887243 1965863 3009230 )
SOURCE_NODE_INDEX=0
NUM_THDS=( 1 )
ITERS=( 1 2 )
BFS_TYPE=( 0 1 )
BFS_TYPE_TEXT=( PUSH PULL )

mkdir $RESULTS_DIR

for data_sets in ${DATA_SETS[*]}
do
	for bfs_type in ${BFS_TYPE[*]}
	do
		for num_thds in ${NUM_THDS[*]}
		do
			echo "make clean"	
			make clean
			echo "make"
			make
			for iters in ${ITERS[*]}
			do
				echo "------------------------ START -------------------------"	

				OUT_FILE=$RESULTS_DIR"/"$PARALLEL_ALGO"_"${SOURCE_NODES[$SOURCE_NODE_INDEX]}"_"$data_sets"_"${BFS_TYPE_TEXT[$bfs_type]}"_"$num_thds"_"$iters".txt"

				echo "./bin/main -f $DATA_SET_DIR/$data_sets -n $num_thds -p $bfs_type -r ${SOURCE_NODES[$SOURCE_NODE_INDEX]} -h > $OUT_FILE"

				TIME_START=$(date +%s)

				./bin/main -f $DATA_SET_DIR/$data_sets -n $num_thds -p $bfs_type -r ${SOURCE_NODES[$SOURCE_NODE_INDEX]} -h > $OUT_FILE

				TIME_END=$(date +%s)
				echo "Run Time: $(( TIME_END - TIME_START ))"

				echo "------------------------- END -------------------------"		
				sleep 1;
			done
		done
	done			
	SOURCE_NODE_INDEX=$(( SOURCE_NODE_INDEX + 1 ))
done
